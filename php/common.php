<?php

function startSession() {
    session_id("myparcel");
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
}
