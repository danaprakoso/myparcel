<?php
//paket yang sudah diterima satpam
include 'db.php';
$adminID = intval($_POST["admin_id"]);
$sort = $_POST["sort"]; //ASC or DESC
$results = $c->query("SELECT * FROM packages WHERE admin_id=" . $adminID . " AND status='sent' ORDER BY date_sent " . $sort);
$values = [];
if ($results && $results->num_rows > 0) {
    while ($row = $results->fetch_assoc()) {
        array_push($values, $row);
    }
}
echo json_encode($values);
