<?php
include 'db.php';
$packageID = intval($_POST["package_id"]);
$results = $c->query("SELECT * FROM images WHERE package_id=" . $packageID);
$values = [];
if ($results && $results->num_rows > 0) {
    while ($row = $results->fetch_assoc()) {
        array_push($values, $row);
    }
}
echo json_encode($values);
